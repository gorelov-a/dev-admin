package com.gorelov.devadmin;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.gorelov.devadmin.*")
public class AppContext {


}
