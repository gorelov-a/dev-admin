package com.gorelov.devadmin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Import;

import java.io.IOException;

@SpringBootApplication
@Import(AppContext.class)
public class DevadminApplication {

    public static void main(String[] args) throws IOException {
        ConfigurableApplicationContext app = SpringApplication.run(DevadminApplication.class, args);
        DevadminService devadminService = app.getBean(DevadminService.class);
        devadminService.craw();
    }
}