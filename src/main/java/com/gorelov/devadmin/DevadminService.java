package com.gorelov.devadmin;

import com.gorelov.devadmin.crawler.Crawler;
import com.gorelov.devadmin.dao.DevadminDao;
import com.gorelov.devadmin.dao.ImageDao;
import com.gorelov.devadmin.domain.Article;
import com.gorelov.devadmin.model.DevadminImage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class DevadminService {

    private Crawler crawler;
    private DevadminDao devadminDao;
    private ImageDao imageDao;

    @Autowired
    public DevadminService(Crawler crawler, DevadminDao devadminDao, ImageDao imageDao) {
        this.crawler = crawler;
        this.devadminDao = devadminDao;
        this.imageDao = imageDao;
    }

    public void craw() throws IOException {
        DevadminImage devadminImage = crawler.extractImage();

        Article insertedArticle = devadminDao.insert(Article.builder().name(devadminImage.getName()).build());
        imageDao.save(String.valueOf(insertedArticle.getId()), devadminImage.getContent());
    }

}