package com.gorelov.devadmin.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

import java.awt.*;

@Getter
@Builder
@AllArgsConstructor
public class DevadminImage {
    private String name;
    private Image content;
}
