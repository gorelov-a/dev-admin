package com.gorelov.devadmin.crawler;

import com.gorelov.devadmin.model.DevadminImage;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

@Component
public class Crawler {
    private URL url;

    public Crawler(@Value("${app.url}") String url) throws MalformedURLException {
        this.url = new URL(url);
    }

    public DevadminImage extractImage() throws IOException {
        Document doc = Jsoup.connect(url.toString()).get();
        String articleName = extractArticleName(doc);
        Image imageContent = extractImageContent(doc);

        return DevadminImage.builder()
                .name(articleName)
                .content(imageContent)
                .build();
    }

    private String extractArticleName(Document document) {
        Elements elements = document.getElementsByClass("firstHeading");

        return elements.first().text();
    }

    private Image extractImageContent(Document document) throws IOException {
        Element parent = document.select("table[class=infobox vcard]").first();
        Element img = parent.getElementsByTag("img").first();

        String src = img.attr("src");

        return ImageIO.read(new URL(url.getProtocol() + ":" + src));
    }
}
