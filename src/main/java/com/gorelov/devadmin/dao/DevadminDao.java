package com.gorelov.devadmin.dao;

import com.gorelov.devadmin.domain.Article;
import org.jooq.DSLContext;
import org.jooq.gorelov.devadmin.Tables;
import org.jooq.gorelov.devadmin.tables.records.ArticleRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DevadminDao {

    private DSLContext dsl;

    @Autowired
    public DevadminDao(DSLContext dsl) {
        this.dsl = dsl;
    }

    public Article insert(Article article) {
        ArticleRecord record = toRecord(article);
        record.store();

        article.setId(record.getId());
        return article;
    }

    public ArticleRecord toRecord(Article article) {
        ArticleRecord record = dsl.newRecord(Tables.ARTICLE);
        record.setName(article.getName());

        Integer id = article.getId();
        if (id != null) {
            record.setId(id);
        }

        return record;
    }
}