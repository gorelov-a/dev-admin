package com.gorelov.devadmin.dao;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

@Repository
public class ImageDao {

    private String path;
    private String imageFormat;

    public ImageDao(@Value("${app.image.path}") String path,
                    @Value("${app.image.format}") String imageFormat) {
        this.path = path;
        this.imageFormat = imageFormat;
    }

    public void save(String fileName, Image image) throws IOException {
        BufferedImage bi = toBufferedImage(image);
        File outputfile = new File(path + "/"+ fileName + "." + imageFormat);
        ImageIO.write(bi, imageFormat, outputfile);
    }

    private static BufferedImage toBufferedImage(Image src) {
        int w = src.getWidth(null);
        int h = src.getHeight(null);
        int type = BufferedImage.TYPE_INT_RGB;
        BufferedImage dest = new BufferedImage(w, h, type);
        Graphics2D g2 = dest.createGraphics();
        g2.drawImage(src, 0, 0, null);
        g2.dispose();
        return dest;
    }
}
