--liquibase 20180421-agorelov sql

--changeset agorelov:1
create table article(
  id SERIAL NOT NULL PRIMARY KEY,
  name VARCHAR (255)
);
--rollback drop table article;